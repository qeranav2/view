<?php
declare(strict_types = 1);

namespace Qeranav2;

use RuntimeException;

class View
{

	public const  PROJECT_DIR = __DIR__ . '/../../';
	/**
	 * @var bool
	 */
	private $is_ajax;
	/**
	 * @var string
	 */
	private $layoutBasePath;


	public function __construct()
	{


		$this->layoutBasePath = self::PROJECT_DIR . '/' . ($_ENV['QERANA_LAYOUT']);
		$server_request       = FILTER_INPUT(INPUT_SERVER, "HTTP_X_REQUESTED_WITH");
		$this->is_ajax        = (isset($server_request) and $server_request === 'XMLHttpRequest');


	}

	/**
	 * @param string $template
	 * @param array $contents
	 * @param $loadNav
	 * @return string
	 */
	public function render(string $template, array $contents = [], $loadNav = true): string
	{


		$templateParts = explode('.', $template);
		$module        = $templateParts[0];
		$templateFile  = $templateParts[1];
		$templatePath  = self::PROJECT_DIR . 'app/' . $module . '/Infrastructure/Ui/html/' . $templateFile . '.php';

		if(!realpath($templatePath)) {
			throw new RuntimeException(sprintf('Unable to locate the template view:%s', $templatePath));
		}

		// processs the parameters is available in the template view
		if(is_array($contents)) {
			foreach ($contents as $key => $valor) {
				$$key = $valor;
			}
		}

		$layout     = 'admin';
		$headerPath = $this->layoutBasePath . '/' . $layout . '/_header.php';
		$footerPath = $this->layoutBasePath . '/' . $layout . '/_footer.php';


		// load header from layout folder
		if(false === $this->is_ajax) {
			if(!realpath($headerPath)) {
				throw new RuntimeException('Layout name %s, does not exists', $layoutPath);
			}
			require($headerPath);
		}

		// include the template
		include($templatePath);

		// load footer
		if(false === $this->is_ajax) {
			if(!realpath($footerPath)) {
				throw new RuntimeException('Layout name %s, does not exists', $layoutPath);
			}
			require($footerPath);
			// include plugins
			require($this->layoutBasePath . '/_plugins.php');

		}

		$var_view = ob_get_contents();
		ob_end_clean();

		return $var_view;

	}


}
